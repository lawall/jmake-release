(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

(* Just do one compilation, not one compilation for every mutation. *)

let iota a b =
  let rec loop n = if n > b then [] else n::loop (n+1) in
  if a > b then [a] else loop a

let rec skip_to_line fn ln = function
    [] -> []
  | n::ns -> if ln > fn n then skip_to_line fn ln ns else n::ns

let id x = x

type patch =
    COMMENT of int (*place*)
  | NONCOMMENT of int (*place*)
  | DEFINE of int (*place*) * string
  | MACRO of (int * string option) (*place*)

let get_messages_for_my_file cfile messages =
  let messages_for_my_file =
    List.fold_left
      (fun p l ->
	match Str.split (Str.regexp ":") l with
	  file::line::col::" error"::_ when file = cfile ->
	    (try
	      let line = int_of_string line in
	      line :: p
	    with _ -> p)
	| _ -> p)
      [] messages in
  messages_for_my_file

let call_mutate cfile o offset = function
    COMMENT(n) ->
      let m = n + offset in
      let label = Printf.sprintf "\"comment:%s:%d:%d\"" cfile (offset+n) n in
      Redo_mutate.comment_line_mutate cfile label n m o;
      (offset,label) (* mutate inline, no change to offset *)
  | NONCOMMENT(n) ->
      let m = n + offset in
      let label = Printf.sprintf "\"noncomment:%s:%d:%d\"" cfile (offset+n) n in
      Redo_mutate.non_comment_line_mutate label n m o;
      (offset+1,label)
  | DEFINE (n,name) ->
      let m = n + offset in
      (let lines =
	Common.cmd_to_list
	  (Printf.sprintf "%s %s | egrep -v \"define[\t ]+%s\""
	     !Flags.grep name name) in
      if lines = []
      then Printf.eprintf "no uses of macro %s\n" name);
      let label = Printf.sprintf "\"define:%s:%d:%d\"" cfile (offset+n) n in
      Redo_mutate.define_line_mutate cfile label n m o;
      (offset,label) (* mutate inline, no change to offset *)
  | MACRO (n,name) ->
      let m = n + offset in
      (match name with
	None -> ()
      | Some name ->
	  let lines =
	    Common.cmd_to_list
	      (Printf.sprintf "%s %s | grep -v define" !Flags.grep name) in
	  if lines = []
	  then Printf.eprintf "no uses of macro %s\n" name);
      let label = Printf.sprintf "\"macro:%s:%d:%d\"" cfile (offset+n) n in
      Redo_mutate.macro_line_mutate label n m o;
      (offset+1,label)

let get_hints topatch =
  List.fold_left
    (fun prev ->
      function DEFINE(_,name) | MACRO(_,Some name) -> name::prev | _ -> prev)
    [] topatch

let inc x = x := !x + 1

type hash_line = AT | INSIDE | NOHASH

let engine patch commit =
  let table = Parse_patch.parse_patch patch in
  let skipped_in_comment = ref 0 in
  let skipped_redundant = ref 0 in
  (let tlen = List.length table in
  if tlen = 1
  then Printf.printf "1: %d C file\n" tlen
  else Printf.printf "1: %d C files\n" tlen);
  (* collect the mutations, for all files *)
  let topatch =
    List.map
      (function (cfile,hunks) ->
	let (hash_lines,macro_lines,define_lines,comment_lines) =
	  Comment_search.comment_search cfile in
	let lines = List.concat(List.map (function (s,e) -> iota s e) hunks) in
	let (topatch ,_,_,_) =
	  List.fold_left
	    (fun (acc,hash_lines,define_lines,comment_lines) ln ->
	      let (incomment,last_comment,comment_lines) =
		match skip_to_line snd ln comment_lines with
		  [] -> (false,false,[])
		| ((start_comment,end_comment)::_) as comment_lines ->
		    if ln > start_comment && ln <= end_comment
		    then
		      if ln = end_comment
		      then (true,true,comment_lines)
		      else (true,false,comment_lines)
		    else (false,false,comment_lines) in
	      let (topatch,hash_lines,define_lines) =
		if not incomment || last_comment
		then
		  try
		    let name = List.assoc ln define_lines in
		    ([DEFINE(ln,name)], hash_lines,
		     List.tl (skip_to_line fst ln define_lines))
		  with Not_found ->
		    (try
		       let name = List.assoc (ln-1) macro_lines in
			  (* MACRO adds mutation with \, needs \ on line
			     before *)
			  (* not perfect due to comments in macros *)
		       (match define_lines with
			 [] ->
			   Printf.eprintf
			     "appears not to be in a macro: %s:%d\n"
			     cfile ln;
			   ([MACRO (ln,name)],hash_lines,define_lines)
		       | def::_ ->
			   if ln < fst def
			   then
			     begin
			       inc skipped_redundant;
			       ([],hash_lines,define_lines)
			     end
			   else
			     ([MACRO (ln,name)],hash_lines,
			      skip_to_line fst ln define_lines))
		     with Not_found ->
		       begin
			 let (danger,hash_lines) =
			   match hash_lines with
			     hash::_ when List.mem ln hash_lines ->
			       (AT,skip_to_line id ln hash_lines)
			   | hash::_ when ln > hash ->
			       (INSIDE,skip_to_line id ln hash_lines)
			   | hash_lines -> (NOHASH,hash_lines) in
			 if not (danger = NOHASH)
			 then
		           (*let ln = if danger = AT then ln + 1 else ln in*)
			   if last_comment
			   then ([COMMENT(ln)],hash_lines,define_lines)
			   else ([NONCOMMENT(ln)],hash_lines,define_lines)
			 else
			   begin
			     inc skipped_redundant;
			     ([],hash_lines,define_lines)
			   end
		       end)
		else
		  begin
		    inc skipped_in_comment;
		    ([],hash_lines,define_lines)
		  end in
	      (topatch@acc,hash_lines,define_lines,comment_lines))
	    ([],hash_lines,define_lines,comment_lines) lines in
	(cfile,List.rev topatch,get_hints topatch))
      table in
  (* create the patch for all files *)
  let file_mutants =
    List.fold_left
      (fun prev (cfile,topatch,hints) ->
	if Bad.badfile cfile
	then
	  begin
	    Printf.printf "File %s cannot be mutated because it may\nbe part of the kernel build process\n"
	      cfile;
	    prev
	  end
	else
	  begin
	    let (tmp,o) = Filename.open_temp_file ("jmake_"^commit) ".patch" in
	    Redo_mutate.start_mutate cfile o;
	    let (_,res) =
	      List.fold_left
		(fun (offset,res) change ->
		  let (o,r) = call_mutate cfile o offset change in (o,r::res))
		(0,[]) topatch in
	    close_out o;
	    (cfile,res,hints,tmp)::prev
	  end)
      [] topatch in
  let (cfiles,hfiles) =
    List.partition (function (file,_,_,_) -> Filename.check_suffix file ".c")
      file_mutants in
  (* Compile *)
  try
    Redo_check_compilation.check_compilation cfiles hfiles;
    List.iter (function (_,_,_,p) -> Sys.remove p) cfiles;
    List.iter (function (_,_,_,p) -> Sys.remove p) hfiles
  with e ->
    (List.iter (function (_,_,_,p) -> Sys.remove p) cfiles;
     List.iter (function (_,_,_,p) -> Sys.remove p) hfiles;
     raise e)
