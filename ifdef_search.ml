(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

type tests =
    Pos of string | Neg of string | Complex of string | NegComplex of string

let hash1 = Str.regexp "[ \t]*#[ \t]*ifdef"
let hash2 = Str.regexp "[ \t]*#[ \t]*ifndef"
let hash3 = Str.regexp "[ \t]*#[ \t]*if"
let hash4 = Str.regexp "[ \t]*#[ \t]*else"
let hash5 = Str.regexp "[ \t]*#[ \t]*elif"
let hash6 = Str.regexp "[ \t]*#[ \t]*endif"

let decomment l =
  let front = List.hd(Str.split (Str.regexp_string "//") l) in
  let pieces1 = Str.split_delim (Str.regexp_string "/*") front in
  let pieces2 =
    List.map (Str.split_delim (Str.regexp_string "*/")) (List.tl pieces1) in
  if List.for_all (fun x -> List.length x = 2) pieces2
  then
    String.concat " "
      ((List.hd pieces1) ::
       (List.map (fun x -> List.nth x 1) pieces2))
  else l

let collect file =
  let i = open_in file in
  let tbl = Hashtbl.create 101 in
  let failed = ref false in
  let check re line =
    match Str.bounded_split_delim re line 2 with
      ["";aft] ->
	let aft = String.trim aft in
	let aft =
	  if Str.string_match (Str.regexp_string "/*") aft 0
	  then decomment aft
	  else aft in
	Some aft
    | _ -> None in
  let stack = ref [] in
  let rec loop n =
    let l = input_line i in
    match check hash1 l with
      Some l ->
	(match Str.split (Str.regexp "[ \t]+") l with
	  x::xs ->
	    stack := (Pos x,n,None) :: !stack;
	    loop (n+1)
	| _ -> failed := true)
    | None ->
	(match check hash2 l with
	  Some l ->
	    (match Str.split (Str.regexp "[ \t]+") l with
	      x::xs ->
		stack := (Neg x,n,None) :: !stack;
		loop (n+1)
	    | _ -> failed := true)
	| None ->
	    (match check hash3 l with
	      Some l ->
		stack := (Complex l,n,None) :: !stack;
		loop (n+1)
	    | None ->
		(match check hash4 l with
		  Some l ->
		    (match !stack with
		      (_,_,Some _)::_ -> failed := true
		    | (v,m,_)::rest ->
			stack := (v,m,Some n)::rest;
			loop (n+1)
		    | _ -> failed := true)
		| None ->
		    (match check hash4 l with
		      Some l ->
			failed := true (* not handling #elif *)
		    | None ->
			(match check hash6 l with
			  Some l ->
			    (match !stack with
			      (v,t,e)::rest ->
				let e =
				  match e with
				    Some e -> e
				  | None -> n in
				Hashtbl.add tbl (t,e,n) v;
				stack := rest;
				loop (n+1)
			    | _ -> failed := true)
			| None -> loop (n+1)))))) in
  (try loop 1 with End_of_file -> close_in i);
  (if not (!stack = [])
  then failed := true);
  if !failed
  then None
  else
    let entries =
      Hashtbl.fold (fun k v r -> (k,v)::r) tbl [] in
    Some(List.sort compare entries)

let neg = function
    Pos x -> Neg x
  | Neg x -> Pos x
  | Complex x -> NegComplex x
  | NegComplex x -> failwith "not possible"

let check_entries entries line =
  let rec loop = function
      [] -> []
    | ((startln,elseln,endln),v)::rest ->
	if startln > line
	then []
	else
	  if startln < line && line < elseln
	  then v :: loop rest
	  else
	    if elseln < line && line < endln
	    then (neg v) :: loop rest
	    else loop rest in
  loop entries
