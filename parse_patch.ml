(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

let normalize s =
  match Str.split (Str.regexp ",") (String.sub s 1 (String.length s - 1)) with
    [n] -> (int_of_string n, int_of_string n)
  | [n;l] -> let n = int_of_string n in (n, n + int_of_string l - 1)
  | _ -> failwith "bad lines"

let collect_lines i =
  let hunks = ref [] in
  let cur = ref [] in
  let file = ref "" in
  let dump _ =
    match !cur with
      [] -> ()
    | _ ->
	let c = !cur in
	cur := [];
	hunks := (!file,List.rev c) :: !hunks in
  let rec loop infile =
    let line = input_line i in
    match Str.split (Str.regexp " ") line with
      ["---";x] ->
	dump();
	let line = input_line i in
        (match Str.split (Str.regexp " ") line with
          ["+++";"/dev/null"] -> loop false (* drop file *)
        | ["++++";fl] -> loop infile
        | ["+++";fl] ->
            let fl = String.sub fl 2 (String.length fl - 2) in
            if (Filename.check_suffix fl ".c" ||
	    Filename.check_suffix fl ".h") &&
	      not (List.mem (List.hd (Str.split (Str.regexp "/") fl))
		     ["Documentation";"scripts";"tools"])
            then begin file := fl; loop true end
            else loop false
        |  _ -> loop false)
    | "@@"::oldn::newn::"@@"::_ ->
        (if infile then cur := (normalize newn) :: !cur);
        loop infile
    | _ -> loop infile in
  try loop false with End_of_file -> dump(); List.rev !hunks

let parse_patch file =
  let i = open_in file in
  let res = collect_lines i in
  close_in i;
  res
