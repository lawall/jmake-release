(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

(* Adapted from Coccinelle, GPLv2 *)
let process_output_to_list2 = fun command ->
  (*Printf.eprintf "command %s\n" command; flush stderr;*)
  let chan = Unix.open_process_in command in
  let res = ref ([] : string list) in
  let rec process_otl_aux () =
    let e = input_line chan in
    res := e::!res;
    process_otl_aux() in
  try process_otl_aux ()
  with End_of_file ->
    let stat = Unix.close_process_in chan in (List.rev !res,stat)
let cmd_to_list command =
  let (l,_) = process_output_to_list2 command in l
let process_output_to_list = cmd_to_list
let cmd_to_list_and_status = process_output_to_list2

let time s f =
  if !Flags.bench
  then
    begin
      let t = Unix.gettimeofday () in
      let res = f() in
      let e = Unix.gettimeofday () in
      Printf.eprintf "%s: %0.2f\n" s (e -. t); flush stderr;
      res
    end
  else f()

let nub l =
  List.rev
    (List.fold_left
       (fun prev cur -> if List.mem cur prev then prev else cur :: prev)
       [] l)

let union l1 l2 =
  List.fold_left
    (fun prev cur -> if List.mem cur prev then prev else cur :: prev)
    l2 (List.rev l1)

let inter l1 l2 =
  List.fold_left
    (fun prev cur -> if List.mem cur l2 then cur :: prev else prev)
    [] l1

let mapi f l =
  let rec loop n = function
      [] -> []
    | x::xs -> (f n x) :: (loop (n+1) xs) in
  loop 0 l

let iteri f l =
  let rec loop n = function
      [] -> ()
    | x::xs -> (f n x); (loop (n+1) xs) in
  loop 0 l

let command s =
   if !Flags.debug
   then
     begin
       Printf.eprintf "---------> %s\n" s;
       flush stderr;
       Sys.command s
     end
   else Sys.command s

let quiet_command s =
   if !Flags.debug
   then
     begin
       Printf.eprintf ".........> %s\n" s;
       flush stderr;
       if !Flags.report
       then Sys.command s
       else Sys.command (Printf.sprintf "%s > /dev/null 2>&1" s)
     end
   else Sys.command (Printf.sprintf "%s > /dev/null 2>&1" s)

let res_command s =
   if !Flags.debug
   then
     begin
       let i = Unix.gettimeofday() in
       let res = cmd_to_list (Printf.sprintf "%s 2> /dev/null" s) in
       let e = Unix.gettimeofday() in
       Printf.eprintf "%s required %0.4f seconds\n" s (e -. i);
       flush stderr;
       res
     end
   else cmd_to_list (Printf.sprintf "%s 2> /dev/null" s)

let stat_command s =
   if !Flags.debug
   then
     begin
       let i = Unix.gettimeofday() in
       let res = cmd_to_list_and_status s in
       let e = Unix.gettimeofday() in
       Printf.eprintf "%s required %0.4f seconds\n" s (e -. i);
       flush stderr;
       res
     end
   else cmd_to_list_and_status s

let safe_commands sl =
   (if !Flags.debug
   then
     begin
       List.iter (fun x -> Printf.eprintf "=========> %s\n" x) sl;
       flush stderr
     end);
  let rec run_many = function
      [] -> None
    | s::ss ->
	let (res,stat) = time s (fun _ -> cmd_to_list_and_status s) in
	if stat = Unix.WEXITED 0
	then run_many ss
	else Some(s,res) in
  let sl = List.map (function s -> s^" 2>&1") sl in
  match run_many sl with
    None -> 0 (* success *)
  | Some(cmd,output) ->
      Printf.eprintf "FAILURE: %s\n" cmd;
      List.iter (function x -> Printf.eprintf "%s\n" x) output;
      Printf.eprintf "retry %s\n" cmd;
      Unix.sleep (Random.int 60);
      match run_many sl with
	None -> 0
      | Some(cmd,output) ->
	  Printf.eprintf "DOUBLE FAILURE: %s\n" cmd;
	  List.iter (function x -> Printf.eprintf "%s\n" x) output;
	  Printf.eprintf "retry %s\n" cmd;
	  Unix.sleep (Random.int 120);
	  match run_many sl with
	    None -> 0
	  | Some(cmd,output) ->
	      Printf.eprintf "TRIPLE FAILURE: %s\n" cmd;
	      List.iter (function x -> Printf.eprintf "%s\n" x) output;
	      let _ = Sys.command "df -kh" in
	      2

type ('a,'b) either = Left of 'a | Right of 'b
