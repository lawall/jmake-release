(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

exception Quit

val try_compilation : string (* make command *) -> string (* .o file *) ->
  string list option

val try_full_compilation : string (* make command *) -> bool

val compile_success : (string,string list ref) Hashtbl.t
val hashadd : ('a,string list ref) Hashtbl.t -> 'a -> string -> unit

val clear_config : unit -> unit
val change_config : string -> int

val get_make : string (* .c file *) ->
  string (* make command *) * string option (* sample file *)

val get_make_with_hints : string (* .c file *) ->
  string list (* hints - in practice macro names *) ->
  string (* make command *) * string option (* sample file *)
