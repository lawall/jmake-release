(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

type input = Range of string * string | Patch of string | Snapshot of string
let input = ref (Patch "")
let git_options = ref ""
let ncores = ref 22
let index = ref 0

let options =
  ["--nogit", Arg.Unit (fun _ -> Flags.grep := "grep -r"), "  no git";
    "--config", Arg.Set_string Flags.config, "  configuration name";
    "-j", Arg.Int (fun n -> Flags.cores := Printf.sprintf "-j %d" n),
    "  number of cores available for full kernel compilations";
    "--ncores", Arg.Set_int ncores,
    "  cores for parallelism";
    "--index", Arg.Set_int index,
    "  identity for parallelism";
    "--git-options", Arg.Set_string git_options,
    "  options for commit selection";
    "--snapshot", Arg.String (function s -> input := Snapshot s),
    "  obtain arch info about files in the current snapshot";
    "--history", Arg.Set_string Flags.history,
    "  files for which make command is known";
    "--work-history", Arg.Set_string Flags.work_history,
    "  commits for which results are known";
    "--linux", Arg.Set_string Flags.linux, "  linux dir [no default]";
    "--debug", Arg.Set Flags.debug, "  debug commands";
    "--tmpdir", Arg.Set_string Flags.tmpdir,
    "  tmp dir for gcc, must exist [default /run/shm/gcctmp]";
    "--extra-configs", Arg.Set_int Flags.extra_configs,
    "  number of configs considered beyond allyesconfig, 0 means all [default 1]";
    "--extra-config-limit", Arg.Set_int Flags.extra_config_limit,
    "  for .h files, number of .c files found before use only allyesconfig [default 100]";
    "--parallel-makes", Arg.Set_int Flags.parallel_makes,
    "  max number of files grouped in a make .i command [default 50]";
  ]

let anonymous s =
  match Str.split (Str.regexp_string "..") s with
    [starter;ender] -> input := Range (starter,ender)
  | _ -> input := Patch s

let usage = ""

let drop_done_commits commits =
  if not (!Flags.work_history = "")
  then
    begin
      let done_commits =
	Common.cmd_to_list
	  (Printf.sprintf "grep \"Working on \" %s/*tmp"
	     !Flags.work_history) in
      Printf.eprintf "got %d done commits\n" (List.length done_commits);
      let done_commits =
	List.fold_left
	  (fun p x -> (List.nth (Str.split (Str.regexp " ") x) 2) :: p)
	  [] done_commits in
      List.fold_left (fun p x -> if List.mem x done_commits then p else x :: p)
	[] commits
    end
  else commits

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  (if not (Sys.file_exists !Flags.tmpdir)
  then failwith (Printf.sprintf "tmp dir %s does not exist" !Flags.tmpdir));
  (if not (!Flags.history = "")
  then
    begin
      let i = open_in !Flags.history in
      let rec loop _ =
	let l = input_line i in
	match Str.split (Str.regexp ": ") l with
	  [file;"failure"] -> loop ()
	| [file;make] ->
	    Quick_check_compilation.hashadd
	      Quick_check_compilation.compile_success
	      file make;
	    loop()
	| _ ->
	    Printf.eprintf "unexpected info: %s\n" l in
      (try loop() with End_of_file -> ());
      close_in i
    end);
  match !input with
    Patch "" -> failwith "some input needed"
  | Patch file -> Quick_engine.engine file file
  | Range(starter,ender) ->
      let patch_file =
	Filename.temp_file (Printf.sprintf "jmake_%d_" !index) ".patch" in
      let args = "-w --diff-filter=M --no-merges" in
      let cmd =
	Printf.sprintf
	  "cd %s%d; git log %s --pretty=format:\"%%h\" %s %s..%s"
	  !Flags.linux !index args !git_options starter ender in
      let commits = Common.cmd_to_list cmd in
      let commits = drop_done_commits commits in
      let commits =
	let rec loop n acc = function
	    [] -> acc
	  | x::xs -> loop (n+1) ((n,x)::acc) xs in
	loop 0 [] commits in
      let anything = List.length commits = 1 in
      Printf.eprintf "todo %d\n" (List.length commits);
      List.iter
	(fun (i,commit) ->
	  if i mod !ncores = !index || anything
	  then
	    begin
	      Sys.chdir (Printf.sprintf "%s%d" !Flags.linux !index);
	      Printf.eprintf "Working on %s\n" commit; flush stderr;
	      let res =
		Common.safe_commands
		  ["git clean -dfx";"git checkout -- .";
		    "git checkout "^commit] in
	      (if not (res = 0) then failwith "git clean etc problem");
	      let start_time = Unix.gettimeofday() in
	      Quick_check_compilation.clear_config();
	      let _ =
		Common.command
		  (Printf.sprintf "git show -U0 %s > %s" commit patch_file) in
	      Quick_engine.engine patch_file commit;
	      let end_time = Unix.gettimeofday() in
	      Printf.eprintf "time: %0.2f\n\n" (end_time -. start_time);
	      flush stdout
	    end)
	commits;
      let _ =
	Sys.command
	  (Printf.sprintf "/bin/rm -f %s*/.git/index.lock" !Flags.linux) in
      (if not !Flags.debug then Sys.remove patch_file);
      ()
  | Snapshot s ->
      Sys.chdir (Printf.sprintf "%s%d" !Flags.linux !index);
      let files = Common.cmd_to_list "find . -name \"*.[ch]\"" in
      let res =
	Common.safe_commands
	  ["git clean -dfx";"git checkout -- .";"git checkout "^s] in
      (if not (res = 0) then failwith "git clean etc problem");
      List.iter
	(function file ->
	  if not (List.mem (List.hd (Str.split (Str.regexp "/") file))
		    ["Documentation";"scripts";"tools"])
	  then
	    begin
	      flush stdout; flush stderr;
	      try
		let (make,candidate_cfile) =
		  Quick_check_compilation.get_make file in
		(match candidate_cfile with
		  Some candidate_cfile when not (file = candidate_cfile) -> 
		    Printf.printf "%s: %s: %s\n" file make candidate_cfile
		| _ -> Printf.printf "%s: %s\n" file make)
	      with
		Quick_check_compilation.Quit ->
		  Printf.printf "%s: failure\n" file
	    end)
	files
