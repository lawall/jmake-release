(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

val comment_search : string (* .c or .h file *) ->
  (int list (* hash lines *) * (int * string option) list (* macro lines *) *
     (int * string) list (* define lines *) *
     (int * int) list (* comment ranges *))
