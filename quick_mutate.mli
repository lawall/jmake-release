(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

(* arguments: target file, target line, output port *)
val start_mutate : string -> out_channel -> unit
val non_comment_line_mutate : int -> int -> out_channel -> unit
val macro_line_mutate : int -> out_channel -> unit
val comment_line_mutate : string -> int -> int -> out_channel -> unit
val define_line_mutate : string -> int -> out_channel -> unit
