(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

(* Before v5.3, make of a single file required a makefile in the current
directory.  A better solution would be to find the version from the
information about the requested commit. *)
let pre_5_3 = false

let makecross = "make.cross" (*"/home/julia/bin/make.cross"*)

let oldconfig = ref ("","")

let sm2tbl = Hashtbl.create 101

let change_config (s,arg) =
  if (s,arg) = !oldconfig
  then Common.safe_commands ["git checkout -- ."]
  else
    begin
      oldconfig := (s,arg);
      Common.safe_commands
	["git checkout -- ."; s ^ " " ^ arg]
    end

(* ---------------------------------------------------------------------- *)

let make_from_arch arch =
  if arch = !Flags.default_architecture
  then "make"
  else Printf.sprintf "%s ARCH=%s" makecross arch

let mktmp mk = Printf.sprintf "TMPDIR=%s %s" !Flags.tmpdir mk
let mktmp2 mk = Printf.sprintf "TMPDIR=%s timeout 360 %s" !Flags.tmpdir mk

let supported_architectures =
  [("x86",["x86_64";"i386"]);("alpha",["alpha"]);("arm",["arm"]);
    ("arm64",["arm64"]);("avr32",["avr32"]);("blackfin",["blackfin"]);
    ("c6x",["c6x"]);("cris",["cris"]);("frv",["frv"]);("h8300",["h8300"]);
    ("hexagon",["hexagon"]);("ia64",["ia64"]);("m32r",["m32r"]);
    ("m68k",["m68k"]);("microblaze",["microblaze"]);("mips",["mips"]);
    ("mn10300",["mn10300"]);("openrisc",["openrisc"]);("parisc",["parisc"]);
    ("powerpc",["powerpc"]);("s390",["s390"]);("score",["score"]);
    ("sh",["sh";"sh64"]);("sparc",["sparc";"sparc32";"sparc64"]);
    ("tile",["tile";"tilepro";"tilegx"]);("um",["um"]);
    ("unicore32",["unicore32"]);("xtensa",["xtensa"])]

let archindex =
  let archs = List.concat (List.map snd supported_architectures) in
  List.map (fun x -> (make_from_arch x,ref 0)) archs

let get_arch_index make =
  try List.assoc make archindex with Not_found -> failwith make
let inc_make make =
  try let i = get_arch_index make in i := !i + 1
  with _ -> Printf.eprintf "unknown make: %s\n" make

let archorder _ =
  let info = List.map (function (arch,ct) -> (!ct,arch)) archindex in
  List.iter
    (function (ct,arch) -> if ct > 0 then Printf.printf "%s: %d\n" arch ct)
    (List.rev (List.sort compare info))

let drop_broken l f =
  List.filter
    (function x ->
      not
	(List.mem (f x)
	   ["arm64";"c6x";"frv";"h8300";"hexagon";"score";"sh64";
	     "sparc32";"tilepro";"unicore32"]))
    l

let from_arch file = (* make is not in the result *)
  match Str.split (Str.regexp "/") file with
    "arch"::arch::_ ->
      (try
	let archs = List.assoc arch supported_architectures in
	let good_archs = drop_broken archs (fun x -> x) in
	(match good_archs with
	  [] ->
	    Some (Common.Right (List.map (fun x -> (x,(x,!Flags.config))) archs))
	| _ -> Some (Common.Left (List.map make_from_arch good_archs)))
      with Not_found -> Some (Common.Right [(arch,(arch,!Flags.config))]))
  | pieces ->
      let pieces = (* surely a set... *)
	List.concat
	  (List.map
	     (fun x ->
	       try List.assoc x supported_architectures
	       with Not_found -> [])
	     pieces) in
      let good_archs = drop_broken pieces (fun x -> x) in
      (match good_archs with
	[] -> None
      | _ -> Some (Common.Left (List.map make_from_arch good_archs)))

let arch_from_arch file = (* make is not in the result *)
  match Str.split (Str.regexp "/") file with
    "arch"::arch::_ ->
      (try
	let archs = List.assoc arch supported_architectures in
	let good_archs = drop_broken archs (fun x -> x) in
	(match good_archs with
	  [] -> Some (Common.Right())
	| _ -> Some (Common.Left [arch]))
      with Not_found -> Some (Common.Right()))
  | pieces ->
      let pieces = (* surely a set... *)
	List.concat
	  (List.map
	     (fun x ->
	       try
		 List.map (fun y -> (x,y))
		   (List.assoc x supported_architectures)
	       with Not_found -> [])
	     pieces) in
      let good_archs = drop_broken pieces snd in
      (match good_archs with
	[] -> None
      | _ -> Some (Common.Left (Common.nub(List.map fst good_archs))))

(* try to ignore irrelevant choices.  duplicates tests, could be more
   efficient *)
let same_arch file choices =
  let choices =
    List.filter (function file -> Filename.check_suffix file ".c") choices in
  match Str.split (Str.regexp "/") file with
    "arch"::arch::_ ->
      List.filter
	(function choice ->
	  match Str.split (Str.regexp "/") choice with
	    "arch"::arch1::_ -> arch = arch1
	  | _ -> true)
	choices
  | _ -> choices

(* ---------------------------------------------------------------------- *)

let get_config line =
  try
    let _ = Str.search_forward (Str.regexp "(CONFIG_[A-Z0-9_]*)") line 0 in
    let res = Str.matched_string line in
    let res = String.sub res 1 (String.length res - 2) in (* drop parens *)
    Some res
  with Not_found -> None

let has_makefile file =
  let makefile = (Filename.dirname file) ^ "/Makefile" in
  if Sys.file_exists makefile
  then Some makefile
  else None

let limit mx l =
  let len = List.length l in
  if mx = 0 || len <= mx
  then l
  else
    let rec loop l ct = function
	0 -> []
      |	n ->
	  let chosen = Random.int ct in
	  let rec iloop l = function
	      0 -> (List.hd l,List.tl l)
	    | n ->
		let (chosen,rest) = iloop (List.tl l) (n-1) in
		(chosen,(List.hd l)::rest) in
	  let (chosen,l) = iloop l chosen in
	  chosen :: loop l (ct-1) (n-1) in
    loop l len mx

let config2arch config subdir fn all num =
  let nospace s = List.length (Str.split (Str.regexp "[ \t]") s) <= 1 in
  let archfiles =
    Common.cmd_to_list
      (Printf.sprintf "%s %s %s arch" !Flags.grep
	 (if all then "-w" else "") (fn config)) in
  let archs =
    List.fold_left
      (fun prev x ->
	match Str.split (Str.regexp "/") x with
	  _::x::_ when nospace x ->
	    if List.mem x prev then prev else x :: prev
	| _ -> prev)
      [] archfiles in
  let configfiles =
    Common.cmd_to_list
      (Printf.sprintf "%s %s %s arch/%s/configs | grep -v :# | cut -d: -f1"
	 !Flags.grep (if all then "-w" else "") (fn config) subdir) in
  let archs_and_targets =
    if num <= !Flags.extra_config_limit
    then
      limit !Flags.extra_configs
	(List.fold_left
	   (fun prev x ->
	     match Str.split (Str.regexp "/") x with
	       _::a::_ when nospace a ->
		 let cfg =
		   List.hd(List.rev (Str.split (Str.regexp "/configs/") x)) in
		 let entry = (a,cfg) in
		 if List.mem entry prev then prev else entry :: prev
	     | _ -> prev)
	   [] configfiles)
    else [] in
  (List.map (function a -> (a,!Flags.config)) archs) @
  archs_and_targets

let backslash = Str.regexp "\\\\[ \t]*"

let get_make_file_lines makefile =
  let checkback line =
    match List.rev (Str.split_delim backslash line) with
      ""::before::_ -> true
    | _ -> false in
  let i = open_in makefile in
  let res = ref [] in
  let rec loop prev =
    let l = input_line i in
    if checkback l
    then loop (l::prev)
    else
      begin
	res := (String.concat " " (List.rev (l::prev))) :: !res;
	loop []
      end in
  try loop [] with End_of_file -> (close_in i; !res)

let archnames a = String.concat " " (Common.nub (List.map fst a))

let get_architectures configs hints num =
  let drop_config s = String.sub s 7 (String.length s - 7) in
  let hints =
    match hints with
      [] -> "*" (* unknown *)
    | [hint] -> hint
    | hints -> Printf.sprintf "{%s}" (String.concat "," hints) in
  let architectures =
    List.fold_left
      (fun prev config ->
	let t1 = config2arch config hints (fun x -> x) true num in
	let t2 _ = config2arch config hints drop_config false num in
	match t1 with
	  [] -> Common.union (t2()) prev
	| _ -> Common.union t1 prev)
      [] configs in
  let (architectures,bad) =
    List.fold_left
      (fun (architectures,bad) arch ->
	let possible =
	  try
	    let archs = List.assoc (fst arch) supported_architectures in
	    Some (List.map (fun x -> (x,snd arch)) archs)
	  with Not_found -> None in
	match possible with
	  Some archs -> (archs@architectures,bad)
	| None -> (architectures,arch::bad))
      ([],[]) architectures in
  match architectures with
    [] ->
      (match bad with
	[] ->
	  (match configs with
	    [] -> Common.Left None
	  | _ -> Common.Left(Some "no archs found"))
      | _ ->
	  Common.Left
	    (Some
	       (Printf.sprintf "possible architectures %s not supported"
		  (archnames bad))))
  | l ->
      match drop_broken l fst with
	[] -> Common.Left (Some (Printf.sprintf "only broken architectures: %s" (String.concat " " (List.map fst l))))
      | l ->
	  let chosen =
	    List.map
	      (fun x -> let arch = make_from_arch (fst x) in (arch,(arch,snd x)))
	      l in
	  let chosen =
	    List.filter
	      (function
		  ("make",("make",all)) when all = !Flags.config -> false | _ -> true)
	      chosen in
	  Common.Right chosen

let search_makefile1 ofile makefile hints num =
  let lines = get_make_file_lines makefile in
  let basefile = Filename.basename ofile in
  let rec loop seen pattern =
    if List.mem pattern seen
    then []
    else
      List.fold_left
	(fun prev line ->
	  match Str.split (Str.regexp "[ \t=]+") line with
	    left::(":"|"+")::right when List.mem pattern right ->
	      let left_pieces =
		List.rev(Str.split (Str.regexp "-") left) in
	      (match left_pieces with
		tail::rest ->
		  let rest =
		    Printf.sprintf "%s.o"
		      (String.concat "-" (List.rev rest)) in
		  let others = loop (pattern::seen) rest in
		  let prev = Common.union others prev in
		  (match get_config tail with
		    Some config ->
		      if List.mem config prev then prev else config :: prev
		  | None -> prev)
	      | [] -> failwith "not possible")
	  | pieces ->
	      if List.mem pattern pieces
	      then
		match get_config line with
		  Some config ->
		    if List.mem config prev then prev else config :: prev
		| None -> prev
	      else prev)
	[] lines in
  let configs = loop [] basefile in
  match get_architectures configs hints num with
    Common.Right chosen -> Common.Right chosen
  | Common.Left msg -> Common.Left msg

(* if needed, we could do this only if the name of the .o file appears in the
Makefile, but maybe filenames can be constructed in makefiles... *)
let search_makefile2 makefile hints num =
  try Hashtbl.find sm2tbl makefile
  with Not_found ->
    begin
      let configs =
	let configs = ref [] in
	let i = open_in makefile in
	let rec loop _ =
	  let line = input_line i in
	  match get_config line with
	    None -> loop ()
	  | Some cfg ->
	      (if not (List.mem cfg !configs)
	      then configs := cfg :: !configs);
	      loop() in
	let res = try loop() with End_of_file -> List.rev !configs in
	close_in i;
	res in
      let res = get_architectures configs hints num in
      Hashtbl.add sm2tbl makefile res;
      res
    end

let header_file_choices file =
  let file_len = String.length file in
  let keep_inc_drop_doc =
    "grep \\#include | grep -v Documentation/ | grep -v scripts/ | grep -v tools/" in
  let subdir =
    let pieces = Str.split (Str.regexp "/") file in
    let rec loop = function
	[] -> "."
      | "include"::prev -> String.concat "/" (List.rev prev)
      | x::prev -> loop prev in
    loop (List.rev pieces) in
  let cmd =
    Printf.sprintf "%s %s %s | grep '\\.c:' | %s"
      !Flags.grep (Filename.basename file) subdir keep_inc_drop_doc in
  let lines = Common.cmd_to_list cmd in
  List.fold_left
    (fun prev line ->
      match Str.split (Str.regexp ":") line with
	cfile::incld::_ ->
	  (match Str.split (Str.regexp "[ \t]+") incld with
	    "#include"::incld::_ ->
	      let ifile =
		match Str.split_delim (Str.regexp "\"") incld with
		  ""::file::_ -> Some file
		| _ ->
		    match Str.split_delim (Str.regexp "[<>]") incld with
		      ""::file::_ -> Some file
		    | _ -> None in
	      (match ifile with
		None -> prev
	      | Some ifile ->
		  let final =
		    List.hd
		      (List.rev(Str.split (Str.regexp_string "../") ifile)) in
		  let flen = String.length final + 1 in
		  let same =
		    try String.sub file (file_len - flen) flen = "/"^final
		    with _ -> false in
		  if same
		  then cfile::prev
		  else prev)
	  | _ -> prev)
      | _ -> prev)
    [] lines
    
(* ---------------------------------------------------------------------- *)

(* options other than the obvious ones *)
let makes_for_cfile cfile print num =
  match has_makefile cfile with
    None -> []
  | Some makefile ->
      let ofile =
	try (Filename.chop_extension cfile) ^ ".o"
	with e -> (Printf.eprintf "bad 1 %s\n" cfile; flush stderr; raise e) in
      let (retry,hints) =
	match arch_from_arch cfile with
	  Some (Common.Left archs) -> (true,archs)
	| Some (Common.Right _) -> (false,[])
	| None -> (true,[]) in
      if retry
      then
	match search_makefile1 ofile makefile hints num with
	  Common.Left None ->
	    (match search_makefile2 makefile hints num with
	      Common.Left _ -> []
	    | Common.Right choices -> choices)
	| Common.Left (Some reason) ->
	    (if print
	    then Printf.printf "2: Local Makefile did not provide any information for %s: %s\n" cfile reason);
	    []
	| Common.Right choices -> choices
      else []

(* obvious options *)
let quick_makes_for_cfile cfile print =
  let try_something _ =
    match from_arch cfile with
      Some (Common.Left makes) -> makes
    | Some (Common.Right arch) ->
	(if print
	then
	  Printf.printf "4: Failure for %s: unsupported architectures %s\n"
	    cfile (archnames arch));
	[]
    | None -> ["make"] in
  let res =
    match has_makefile cfile with
      None ->
      (* make foo.o doesn't work if there is no makefile in in
         the directory *)
	if pre_5_3
	then
	  begin
	    (if print then Printf.printf "3: the directory of %s contains no makefile\n" cfile);
	    []
	  end
	else try_something()
    | Some makefile -> try_something() in
  List.map (function x -> (x,(x,!Flags.config))) res

let notin l x = not (List.mem x l)

let cfiles_for_hfile hfile hints =
  if !Flags.check_headers
  then
    let possibilities = from_arch hfile in
    match possibilities with
      Some (Common.Right arch) ->
	Printf.printf "5: %s: unsupported architectures %s\n" hfile
	  (archnames arch);
	(None,[])
    | _ ->
	let hfc = header_file_choices hfile in
	let hfc = same_arch hfile hfc in
	let hint_choices =
	  List.map
	    (function hint ->
	      let cmd = Printf.sprintf "git grep -w --files-with-matches %s"
		  hint in
	      same_arch hfile (Common.cmd_to_list cmd))
	    hints in
	let hint_inter =
	  match hint_choices with
	    [] -> []
	  | [hint_choices] -> hint_choices
	  | f::rest -> List.fold_left Common.inter f rest in
	let best = Common.inter hfc hint_inter in
	let good = List.filter (notin best) hint_inter in
	let remaining =
	  let used = best @ good in
	  let hfc = List.filter (notin used) hfc in
	  let hint_choices = List.map (List.filter (notin used)) hint_choices in
	  List.fold_left Common.union hfc hint_choices in
	(match possibilities with
	  Some (Common.Left makes) ->
	    let makes = List.map (fun x -> (x,(x,"allyesconfig"))) makes in
	    (Some makes,best@good@remaining)
	| _ -> (None,best@good@remaining))
  else (None,[])
      
(* ---------------------------------------------------------------------- *)
      
let apply_patch = function
    "" -> ()
  | patch ->
      let cmd = Printf.sprintf "patch -F0 -p1 < %s" patch in
      let x = Common.quiet_command cmd in
      (if !Flags.debug
      then
	let patch = Common.cmd_to_list ("cat "^patch) in
	List.iter (fun x -> Printf.eprintf "%s\n" x) patch);
      (if not (x = 0) then failwith ("bad apply: "^cmd));
      ()

let unapply_patch str = function
    "" -> ()
  | patch ->
      let cmd = Printf.sprintf "patch -R -p1 < %s" patch in
      let x = Common.quiet_command cmd in
      (if not (x = 0)
      then failwith (Printf.sprintf "bad unapply: %s: %s" str cmd));
      ()

let get_patches = List.map (function (_,_,_,p) -> p)
let get_names = List.map (function (p,_,_,_) -> p)

let rec afterfl fl = function
    [] -> []
  | x::xs ->
      let pieces = Str.split (Str.regexp "[ \t]+") x in
      if List.mem fl pieces && List.mem "CC" pieces
      then xs
      else afterfl fl xs

(* need to see if the unmutated code compiles even if it doesn't use up all
of what is wanted *)
let paper_eval = true

let apply_make cfile wanted cpatch hfiles htoundo (make,(m,config)) termination =
  let owanted = wanted in
  let start =
    try Filename.chop_extension cfile
    with e -> (Printf.eprintf "bad 2 %s\n" cfile; flush stderr; raise e) in
  let ofile = start ^ ".o" in
  let ifile = start ^ ".i" in
  if Sys.file_exists ifile
  then
    begin
      (if not (make = "make") then inc_make make);
      let lines =
	Common.cmd_to_list
	  (Printf.sprintf "grep ¤ %s; /bin/rm -f %s" ifile ifile) in
      let (newwanted,extra) =
	List.fold_left
	  (fun (wanted,extra) l ->
	    let pieces = Str.split_delim (Str.regexp "¤") l in
	    let relevant =
	      let rec loop = function
		  [] | [_] -> []
		| _::n::rest -> n :: loop rest in
	      loop pieces in
	    let (inwanted,other) =
	      List.partition (function x -> List.mem x wanted) relevant in
	    let wanted =
	      List.filter (function x -> not (List.mem x inwanted)) wanted in
	    let extra = Common.union other extra in
	    (wanted,extra))
	  (wanted,[]) lines in
      let reduced = List.length wanted > List.length newwanted in
      if reduced || newwanted = [] || paper_eval
      then
	begin
	  let (incomplete_headers,succeeded,changed,thunks) =
	    List.fold_left
	      (fun (prev,succeeded,changed,thunks) (hfile,orange,hints,patch) ->
		let (range,found) = List.partition (notin extra) orange in
		let thunks = (* only print on compile success *)
		  (fun _ ->
		    Printf.printf "6: %s %s: hfile %s: %d -> %d\n" m config hfile
		      (List.length orange) (List.length range); flush stdout)
		  :: thunks in
		if range = []
		then (* done with this header file *)
		  (prev,hfile::succeeded,true,thunks)
		else
		  ((hfile,range,hints,patch)::prev,succeeded,
		   changed || not(found = []),thunks))
	      ([],[],false,[]) hfiles in
	  if termination changed
	  then
	    begin
	  (* check if it really compiles *)
	      unapply_patch "cpatch" cpatch;
	      List.iter (unapply_patch "hfile patches") (get_patches htoundo);
	      let werror = if !Flags.warnok then "" else "KCFLAGS=-Werror" in
	      let (err,stat) =
		Common.time "makeo"
		  (fun _ ->
		    Common.stat_command
		      (Printf.sprintf "%s %s -j2 %s 2>&1"
			 werror (mktmp make) ofile)) in
	      let feedback =
		if !Flags.warnok
		then
		    let res = afterfl ofile err in
		    if res = []
		    then ""
		    else " with warnings\n" ^ (String.concat "\n" res) ^ "\n"
		else "" in
	      (if Sys.file_exists ofile then Sys.remove ofile);
	      List.iter apply_patch (get_patches incomplete_headers);
	      if stat = Unix.WEXITED 0
	      then (* yippee, it works *)
		if not (reduced || wanted = [])
		then
		  begin (* only get here if paper_eval is true *)
		    (* must be cfile case, not hfile case, if wanted != [] *)
		    Printf.printf
		      "7: %s %s: %s compiled%s but %d/%d mutants remain\n"
		      m config cfile feedback
		      (List.length wanted) (List.length owanted);
		    Common.Right (incomplete_headers,wanted)
		  end
		else
		  begin
		    List.iter (function th -> th()) thunks;
		    (if reduced
		    then
		      begin
			Printf.printf
			  "8: %s %s: cfile %s:%s %d -> %d\n" m config cfile
			  feedback (List.length wanted) (List.length newwanted);
			flush stdout
		      end);
		    List.iter
		      (function hfile ->
			Printf.printf "9: Success for %s (%s %s%s)\n"
			  hfile m config feedback)
		      succeeded;
		    Common.Left (incomplete_headers,newwanted)
		  end
	      else (* oops, have to try again *)
		begin
		  (if !Flags.report
		  then
		    begin
		      Printf.printf "ofile: %s\n" ofile;
		      let err =
			List.filter
			  (function x ->
			    try
			      let _ =
				Str.search_forward (Str.regexp "error: ")
				  x 0 in true
			    with Not_found -> false)
			  err in
		      List.iter (function x -> Printf.printf "  %s\n" x) err;
		      flush stdout
		    end);
		  Common.Right (incomplete_headers,[".o compile failure"])
		end
	    end
	  else Common.Right (incomplete_headers,["header not completely addressed"])
	end
      else (* no change, try something else *)
	Common.Right (htoundo,wanted)
    end
  else Common.Right (htoundo,["compile failure"])
  
let run_one_make remaining_hfiles cfiles make limit termination htermination k =
  let success = change_config (snd make) in
  if not (success = 0)
  then (remaining_hfiles,cfiles) (* failed *)
  else
    begin
      let hpatches = List.map (fun (_,_,_,p) -> p) remaining_hfiles in
      List.iter apply_patch hpatches;
      let cfiles =
	let rec loop acc = function
	    [] -> if acc = [] then [] else [List.rev acc]
	  | f::fs ->
	      if List.length acc + 1 = limit
	      then (List.rev (f::acc)) :: (loop [] fs)
	      else loop (f::acc) fs in
	loop [] cfiles in
      let rec loop remaining_hfiles htoundo remaining_cfiles = function
	  [] -> (remaining_hfiles,remaining_cfiles)
	| cfiles::rest ->
	    let cpatches = List.map (fun (_,_,_,p) -> p) cfiles in
	    List.iter apply_patch cpatches;
	    let ifiles =
	      List.map (function (x,_,_,_) -> Filename.chop_extension x ^ ".i")
		cfiles in
	    let cmd =
	      Printf.sprintf "%s %s-k %s"
		(fst make) (if List.length ifiles > 1 then "-j2 " else "")
		(String.concat " " ifiles) in
	    ignore(Common.time "makeis" (fun _ -> Common.quiet_command (mktmp2 cmd)));
	    let rec iloop remaining_hfiles htoundo remaining_cfiles = function
		[] -> loop remaining_hfiles htoundo remaining_cfiles rest
	      | ((cfile,range,h,patch) as c)::cfiles ->
		  let res =
		    apply_make cfile range patch remaining_hfiles htoundo make
		      htermination in
		  match res with
		    Common.Left (headers,[]) -> (* success for this C file *)
		      k make cfile;
		      if termination make cfile headers
		      then (headers,remaining_cfiles)
		      else
			begin
		      (* apply_make unapplied hfile patches *)
			  List.iter apply_patch (get_patches headers);
			  iloop headers headers remaining_cfiles cfiles
			end
		  | Common.Left (headers,wanted) -> (* success for this C file *)
		      iloop headers headers ((cfile,wanted,h,patch)::remaining_cfiles)
			cfiles
		  | Common.Right (htoundo,best) -> (* failure for this C file *)
		      iloop remaining_hfiles htoundo (c::remaining_cfiles) cfiles in
	    iloop remaining_hfiles htoundo remaining_cfiles cfiles in
      loop remaining_hfiles remaining_hfiles [] cfiles
    end
      
let popular_makes l =
  let tbl = Hashtbl.create 101 in
  let hashadd k =
    let cell =
      try Hashtbl.find tbl k
      with Not_found ->
	let cell = ref 0 in
	Hashtbl.add tbl k cell;
	cell in
    cell := !cell + 1 in
  List.iter (function (_,makes) -> List.iter hashadd makes) l;
  tbl

let cctr = ref 0

let klocalizer_exists =
  List.length (Common.cmd_to_list "which klocalizer") = 1

let get_slow_make_env cfiles =
  let cmakeinfo =
    List.map
      (function (cfile,range,_,patch) ->
	let ofile =
	  try (Filename.chop_extension cfile) ^ ".o"
	  with e ->
	    (Printf.eprintf "bad 1 %s\n" cfile; flush stderr; raise e) in
	let infos = Ifdef_search.collect cfile in
	let requirements =
	  match infos with
	    Some infos ->
	      let lines =
		List.map
		  (function x -> (* a bit clunky, but need to get what is matched from compiler output *)
		    match Str.split (Str.regexp ":") x with
		      [ty;fl;ln;oln] -> int_of_string ln
		    | _ -> failwith "bad line structure")
		  range in
	      if !Flags.debug
	      then
		begin
		  let requirements =
		    List.map
		      (fun line -> (line, Ifdef_search.check_entries infos line))
		      lines in
		  Printf.eprintf "%s:\n" cfile;
		  List.iter
		    (function (ln,r) ->
		      Printf.eprintf "   %d:\n" ln;
		      List.iter
			(function
			    Ifdef_search.Pos x -> Printf.eprintf "     %s\n" x
			  | Ifdef_search.Neg x -> Printf.eprintf "     not %s\n" x
			  | Ifdef_search.Complex x -> Printf.eprintf "     %s\n" x
			  | Ifdef_search.NegComplex x -> Printf.eprintf "     not(%s)\n" x)
			r)
		    requirements;
		  Printf.eprintf "\n"
		end;
	      List.fold_left
		(fun prev line ->
		  let cur =
		    List.sort compare (Ifdef_search.check_entries infos line) in
		  let cur =
		    List.filter
		      (function
			  Ifdef_search.Pos x | Ifdef_search.Neg x -> true
			| _ -> false (* not supported for the moment *))
		      cur in
		  if List.mem cur prev then prev else cur :: prev)
		[] lines
	  | None -> [[]] in
	let res =
	  if !Flags.use_klocalizer && klocalizer_exists
	  then
	    List.fold_left
	      (fun prev requirement ->
		let args =
		  String.concat ""
		    (List.map
		       (function
			   Ifdef_search.Pos x -> Printf.sprintf "-D %s " x
			 | Ifdef_search.Neg x -> Printf.sprintf "-U %s " x
			 | _ -> failwith "not possible")
		       requirement) in
		let kmax_try =
		  Common.res_command (Printf.sprintf "klocalizer -q %s%s" args ofile) in
		match kmax_try with
		  [arch] ->
		    (match drop_broken kmax_try (fun x -> x) with
		      [] -> []
		    | [arch] ->
			let c = !cctr in
			cctr := !cctr + 1;
			let fl = Printf.sprintf "tmp_config%d" c in
			ignore (Sys.command (Printf.sprintf "cp .config %s" fl));
Printf.eprintf ".config -> %s in %s has %s lines\n" fl (String.concat " " (Common.cmd_to_list "pwd")) (String.concat " " (Common.cmd_to_list "wc -l .config")); flush stderr;
			let mk = make_from_arch arch in
			(mk,(Printf.sprintf "cp %s .config; %s" fl mk,"olddefconfig"))::prev
		    | _ -> failwith "not possible")
		| [] -> prev
		| l -> failwith (Printf.sprintf "kmax returned:\n%s\n" (String.concat "\n" l)))
	      []
	      requirements
	  else [] in
	let res =
	  let mks =
	    List.fold_left
	      (fun prev (mk,_) ->
		let proposed = (mk,(mk,!Flags.config^" # kmax")) in
		if List.mem proposed prev then prev else proposed :: prev)
	      [] res in
	  res @ mks in
	(cfile,res))
      cfiles in
  let allmakes = List.fold_left Common.union [] (List.map snd cmakeinfo) in
  (allmakes,cmakeinfo)

let get_make_env cfiles print =
  let num = List.length cfiles in
  let cmakeinfo =
    List.map
      (function (cfile,range,_,patch) ->
	(cfile,makes_for_cfile cfile print num))
      cfiles in
  let tbl = popular_makes cmakeinfo in
  let allmakes = List.fold_left Common.union [] (List.map snd cmakeinfo) in
  let counted =
    List.map
      (fun x ->
	((!(get_arch_index (fst x)),!(Hashtbl.find tbl x)),x))
      allmakes in
  let allmakes = List.map snd (List.rev (List.sort compare counted)) in
  let (mk,others) = List.partition (fun x -> (fst x) = "make") allmakes in
  (mk @ others,cmakeinfo)

let get_quick_make_env cfiles print =
  let cmakeinfo =
    List.map
      (function (cfile,range,_,patch) ->
	(cfile,quick_makes_for_cfile cfile print))
      cfiles in
  let tbl = popular_makes cmakeinfo in
  let allmakes = List.fold_left Common.union [] (List.map snd cmakeinfo) in
  let counted =
    List.map
      (fun x ->
	((!(get_arch_index (fst x)),!(Hashtbl.find tbl x)),x))
      allmakes in
  let allmakes = List.map snd (List.rev (List.sort compare counted)) in
  let (mk,others) = List.partition (fun x -> (fst x) = "make") allmakes in
  (mk @ others,cmakeinfo)

let inter l1 l2 = List.filter (fun x -> List.mem x l2) l1

let makemerge qmakeinfo cmakeinfo =
  match List.partition (fun (fl,makes) -> makes = []) cmakeinfo with
    ([],_) | (_,[]) -> cmakeinfo (* have info for everyone or no idea *)
  | (missing,ok) ->
      (* if all oks have the same thing, then try that for files with no
	 info *)
      let okinfo =
	List.map
	  (fun (fl,cur) ->
	    (fl,(try List.assoc fl qmakeinfo with Not_found -> [])@cur))
	  ok in
      let options =
	List.fold_left inter (snd (List.hd okinfo))
	  (List.map snd (List.tl okinfo)) in
      (List.map (fun (fl,_) -> (fl,options)) missing) @ ok

let check_compilation cfiles hfiles =
  oldconfig := ("",""); (* new patch, so start over *)
  Hashtbl.clear sm2tbl;
  (* Process each .c file *)
  let rec loop remaining_hfiles remaining_cfiles cmakeinfo = function
      [] -> (remaining_hfiles,remaining_cfiles)
    | _ when remaining_cfiles = [] -> (remaining_hfiles,remaining_cfiles)
    | make::makes ->
	let (totest,remaining_cfiles) =
	  List.partition
	    (function (cfile,_,_,_) ->
	      List.mem make (List.assoc cfile cmakeinfo))
	    remaining_cfiles in
	let (remaining_hfiles,rc) =
	  run_one_make remaining_hfiles totest make !Flags.parallel_makes
	    (fun _ _ _ -> false) (fun _ -> true)
	    (fun make cfile ->
	      Printf.printf "10: Success for %s (%s)\n" cfile
		(if snd(snd make) = "allyesconfig"
		then fst make
		else Printf.sprintf "%s:%s" (fst (snd make)) (snd (snd make)))) in
	loop remaining_hfiles (rc@remaining_cfiles) cmakeinfo makes in
  let remaining_hfiles =
    (if !Flags.debug then Printf.printf "-------- Quick makefile options --------\n");
    let (allmakes,cmakeinfo) = get_quick_make_env cfiles true in
    let (remaining_hfiles,remaining_cfiles) =
      loop hfiles cfiles cmakeinfo allmakes in
    let qmakeinfo = cmakeinfo in
    if remaining_cfiles = []
    then remaining_hfiles
    else
      ((if !Flags.debug then Printf.printf "-------- Makefile based options --------\n");
      let (allmakes,cmakeinfo) =
	Common.time "get_make_env"
	  (fun _ -> get_make_env remaining_cfiles true) in
      let cmakeinfo = makemerge qmakeinfo cmakeinfo in
      let (remaining_hfiles,remaining_cfiles) =
	loop remaining_hfiles remaining_cfiles cmakeinfo allmakes in
      if remaining_cfiles = []
      then remaining_hfiles
      else
	((if !Flags.debug then Printf.printf "-------- Klocalizer based options --------\n");
	let (allmakes,cmakeinfo) =
	  Common.time "get_slow_make_env"
	    (fun _ -> get_slow_make_env remaining_cfiles) in
	let (remaining_hfiles,remaining_cfiles) =
	  loop remaining_hfiles remaining_cfiles cmakeinfo allmakes in
	begin
	  List.iter
	    (function (cfile,wanted,_,_) ->
	      let (_,owanted,_,_) =
		List.find (function (c,_,_,_) -> c = cfile) cfiles in
	      Printf.printf "11: Failure for %s: %d/%d mutants remain\n"
		cfile (List.length wanted) (List.length owanted);
	      List.iter
		(function mut -> Printf.printf "%s\n" mut)
		(List.sort compare wanted))
	    remaining_cfiles;
	  remaining_hfiles
	end)) in
  flush stdout; flush stderr;
  Printf.printf "-----------------------------------------------------\n";
  flush stdout; flush stderr;
  (* Process any remaining .h files *)
  let makes_and_cfiles =
    List.map
      (function ((hfile,range,hints,patch) as h) ->
	(h,cfiles_for_hfile hfile hints))
      remaining_hfiles in
  let tbl = Hashtbl.create 101 in
  let hashadd k v =
    let cell =
      try Hashtbl.find tbl k
      with Not_found ->
	let cell = ref [] in
	Hashtbl.add tbl k cell;
	cell in
    cell := v :: !cell in
  List.iter
    (function ((h,_,_,_) as hfile,(mk,cfiles)) ->
      Printf.printf "12: %s: %d cfiles to choose from\n" h
	(List.length cfiles);
      List.iter
	(function f -> Printf.printf "  %s\n" f)
	cfiles; flush stdout;
      match mk with
	None -> hashadd None (hfile,cfiles)
      | Some mk -> hashadd (Some mk) (hfile,cfiles))
    makes_and_cfiles;
  let options =
    Hashtbl.fold
      (fun k v r ->
	(k,List.map fst !v,List.fold_left Common.union [] (List.map snd !v))
	  :: r)
      tbl [] in
  let rec loop remaining_hfiles cfiles cmakeinfo = function
      [] -> remaining_hfiles
    | _ when remaining_hfiles = [] -> remaining_hfiles
    | make::makes ->
	let (totest,remaining_cfiles) =
	  List.partition (function (cfile,_,_,_) -> cmakeinfo make cfile)
	    cfiles in
	let (remaining_hfiles,_) =
	  run_one_make remaining_hfiles totest make !Flags.parallel_makes
	    (fun make cfile new_hfiles -> new_hfiles = [])
	    (fun changed -> changed)
	    (fun make cfile -> ()) in
	loop remaining_hfiles cfiles cmakeinfo makes in
  (* could consider the possibility that .c files are included, or that
     the .c files we find for .h files are included.  but we don't. *)
  List.iter (* hfiles in each entry must be disjoint *)
    (function (mks,new_hfiles,cfiles) ->
      let cfiles = List.map (function cfile -> (cfile,[],[],"")) cfiles in
      let (makes,cmakeinfo) =
	match mks with
	  Some mks -> (mks,fun make x -> true)
	| None ->
	    let (allmakes,cmakeinfo) = get_quick_make_env cfiles false in
	    (allmakes,fun make x -> List.mem make (List.assoc x cmakeinfo)) in
      let make_count1 = List.length makes in
      match loop new_hfiles cfiles cmakeinfo makes with
	[] -> () (* success *)
      | remaining_hfiles ->
	  let (makes,cmakeinfo) =
	    match mks with
	      Some mks -> ([],fun make x -> true)
	    | None ->
		let (allmakes,cmakeinfo) =
		  Common.time "get_make_env2"
		    (fun _ -> get_make_env cfiles false) in
		(allmakes,
		 fun make x -> List.mem make (List.assoc x cmakeinfo)) in
	  let make_count2 = List.length makes in
	  (if make_count1 + make_count2 = 0
	  then
	    Printf.printf "13: for %d hfiles, no makeable cfile obtained\n"
	      (List.length new_hfiles));
	  match loop remaining_hfiles cfiles cmakeinfo makes with
	    [] -> () (* success *)
	  | remaining_hfiles ->
	      List.iter
		(function (hfile,range,_,_) ->
		  let (_,wanted,_,_) =
		    List.find (function (h,_,_,_) -> h = hfile) hfiles in
		  Printf.printf
		    "14: Failure for %s: %d/%d mutants remain\n"
		    hfile (List.length range) (List.length wanted);
		  List.iter
		    (function mut -> Printf.printf "%s\n" mut)
		    (List.sort compare range))
		remaining_hfiles)
    options
