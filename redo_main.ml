(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

type input = Range of string * string | Patch of string | List of string list
let input = ref (Patch "")
let git_options = ref ""
let ncores = ref 1(*22*)
let index = ref 0
let whitespace = ref false
let file = ref ""

let options =
  ["--nogit", Arg.Unit (fun _ -> Flags.grep := "grep -r"), "  no git";
    "--config", Arg.Set_string Flags.config, "  configuration name";
    "-j", Arg.Int (fun n -> Flags.cores := Printf.sprintf "-j %d" n),
    "  number of cores available for full kernel compilations";
    "--ncores", Arg.Set_int ncores,
    "  cores for parallelism";
    "--index", Arg.Set_int index,
    "  identity for parallelism";
    "--git-options", Arg.Set_string git_options,
    "  options for commit selection";
    "--work-history", Arg.Set_string Flags.work_history,
    "  commits for which results are known";
    "--linux", Arg.Set_string Flags.linux, "  linux dir";
    "--tmpdir", Arg.Set_string Flags.tmpdir, "  temp dir";
    "--file", Arg.Set_string file, "  file of interest";
    "--debug", Arg.Set Flags.debug, "  debug commands";
    "--bench", Arg.Set Flags.bench, "  running time information";
    "--extra-configs", Arg.Set_int Flags.extra_configs,
    "  configs considered other than allyesconfig";
    "--whitespace", Arg.Set whitespace, "  significant whitespace";
    "--warnok", Arg.Set Flags.warnok, "  allow success on warnings";
    "--report", Arg.Set Flags.report, "  print compiler output";
    "--no-klocalizer", Arg.Clear Flags.use_klocalizer, "  don't use klocalizer, even if it exists";
    "--no-headers", Arg.Clear Flags.check_headers, "  make no special effort to compile header files"]

let anonymous s =
  match Str.split (Str.regexp_string "..") s with
    [starter;ender] -> input := Range (starter,ender)
  | _ ->
      match Str.split (Str.regexp_string ",") s with
	(a::b::_) as l -> input := List l
      | _ ->
	  if Sys.file_exists s
	  then input := Patch s
	  else input := Range(s^"^",s)

let usage = ""

let drop_done_commits commits =
  if not (!Flags.work_history = "")
  then
    begin
      let done_commits =
	Common.cmd_to_list
	  (Printf.sprintf "grep \"Working on \" %s/*tmp"
	     !Flags.work_history) in
      Printf.eprintf "got %d done commits\n" (List.length done_commits);
      let done_commits =
	List.fold_left
	  (fun p x -> (List.nth (Str.split (Str.regexp " ") x) 2) :: p)
	  [] done_commits in
      List.fold_left (fun p x -> if List.mem x done_commits then p else x :: p)
	[] commits
    end
  else commits

let commit_list commits ldir =
  let commits = drop_done_commits commits in
  let commits =
    let rec loop n acc = function
	[] -> acc
      | x::xs -> loop (n+1) ((n,x)::acc) xs in
    loop 0 [] commits in
  let anything = List.length commits = 1 in
  Printf.eprintf "commits %d\n" (List.length commits);
  (if List.length commits < !ncores then ncores := List.length commits);
  let patch_file =
    Filename.temp_file (Printf.sprintf "jmake_%d_" !index) ".patch" in
  List.iter
    (fun (i,commit) ->
      if i mod !ncores = !index || anything
      then
	begin
	  Sys.chdir (Printf.sprintf "%s" ldir);
	  Printf.eprintf "Working on %s\n" commit; flush stderr;
	  let res =
	    Common.safe_commands
	      ["git clean -dfx";"git reset --hard "^commit] in
	  (if not (res = 0) then failwith "git clean etc problem");
	  let start_time = Unix.gettimeofday() in
	  let _ =
	    Common.command
	      (Printf.sprintf "git show %s -U0 %s %s > %s"
		 (if !whitespace then "" else "-w")
		 commit
		 (if !file = "" then "" else (" -- " ^ !file))
		 patch_file) in
	  Redo_engine.engine patch_file commit;
	  let end_time = Unix.gettimeofday() in
	  Printf.eprintf "time: %0.2f\n\n" (end_time -. start_time);
	  flush stdout
	end)
    commits;
  let _ =
    Sys.command
      (Printf.sprintf "/bin/rm -f %s*/.git/index.lock" !Flags.linux) in
  (if not !Flags.debug then Sys.remove patch_file);
  flush stdout; flush stderr;
  Redo_check_compilation.archorder()

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  Random.self_init(); (* for file subset selection *)
  let ldir =
    let ldir = Printf.sprintf "%s/linux%d" !Flags.tmpdir !index in
    (if not (Sys.file_exists !Flags.tmpdir)
    then let _ = Sys.command ("mkdir -p "^(!Flags.tmpdir)) in ());
    let _ =
      Sys.command
	(Printf.sprintf "/bin/rm -rf %s; cp -r %s %s" ldir !Flags.linux
	   ldir) in
    ldir in
  Printf.eprintf "temporary dir: %s\n" ldir;
  let args = "-w --diff-filter=AM --no-merges" in
  match !input with
    Patch "" -> failwith "some input needed"
  | Patch file -> Redo_engine.engine file file
  | Range(starter,ender) ->
      let cmd =
	Printf.sprintf
	  "cd %s; git log %s --pretty=format:\"%%h\" %s %s..%s"
	  ldir args !git_options starter ender in
      commit_list (Common.cmd_to_list cmd) ldir
  | List commits ->
      let cmd c =
	Printf.sprintf
	  "cd %s; git log %s --pretty=format:\"%%h\" %s %s^..%s"
	  ldir args !git_options c c in
      commit_list
	(List.concat
	   (List.map (function c -> Common.cmd_to_list (cmd c)) commits))
	ldir

