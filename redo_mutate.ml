(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

(* Creates a patch that does the required mutation on the existing code *)

let start_mutate file o =
  Printf.fprintf o "--- a/%s\n" file;
  Printf.fprintf o "+++ b/%s\n" file

let non_comment_line_mutate ct n m o =
  Printf.fprintf o "@@ -%d,0 +%d @@\n" (n-1) m;
  Printf.fprintf o "+¤%s¤\n" ct

let macro_line_mutate ct n m o =
  Printf.fprintf o "@@ -%d,0 +%d @@\n" (n-1) m;
  Printf.fprintf o "+¤%s¤\\\n" ct

(* ----------------------------------------------------------------------- *)

let normalize s =
  match Str.split (Str.regexp ",") (String.sub s 1 (String.length s - 1)) with
    [n] -> (int_of_string n, int_of_string n)
  |  [n;l] -> let n = int_of_string n in (n, n + int_of_string l - 1)
  |  _ -> failwith "bad lines"

let minus = Str.regexp_string "-"

let get_line file n =
  let i = open_in file in
  let rec loop n =
    let l = input_line i in
    if n = 1
    then l
    else loop (n-1) in
  let res = loop n in
  close_in i;
  res

let c_comment_ender = Str.regexp_string "*/"

let add_char_after_comment char l =
  match Str.split_delim c_comment_ender l with
    before::after -> before ^ "*/" ^ char ^ (String.concat "*/" after)
  | _ -> failwith "not really a comment line"

let comment_line_mutate file ct n m o =
  let l = get_line file n in
  Printf.fprintf o "@@ -%d +%d @@\n" n m;
  Printf.fprintf o "-%s\n" l;
  let char = Printf.sprintf "¤%s¤" ct in
  Printf.fprintf o "+%s\n" (add_char_after_comment char l)

let untext l =
  String.concat ""
    (List.map (function Str.Text s -> s | Str.Delim s -> s) l)

let backslash = Str.regexp "\\\\[ \t]*"
let add_char_near_end char l =
  match List.rev(Str.full_split backslash l) with
    Str.Text "" :: Str.Delim d :: rest ->
      (untext (List.rev rest)) ^ char ^ d
  | Str.Delim d :: rest -> (* not sure it can occur *)
      (untext (List.rev rest)) ^ char ^ d
  | _ -> l ^ char

let define_line_mutate file ct n m o =
  let l = get_line file n in
  Printf.fprintf o "@@ -%d +%d @@\n" n m;
  Printf.fprintf o "-%s\n" l;
  let char = Printf.sprintf "¤%s¤" ct in
  Printf.fprintf o "+%s\n" (add_char_near_end char l)
