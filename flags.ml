(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

let grep = ref "git grep" (* alternate: grep -r *)

let config = ref "allyesconfig"

let cores = ref ""

let default_architecture = ref "x86_64"

let linux = ref (Sys.getcwd())
let history = ref "/home/jll/papers/jmake/tools/archinfo"
let work_history = ref "" (*"/home/jll/papers/jmake/results"*)

let debug = ref false
let bench = ref false

let tmpdir = ref "/run/shm/gcctmp"

(* limit on the number of configurations considered other than allyesconfig *)
let extra_configs = ref 1

let parallel_makes = ref 50

let extra_config_limit = ref 100

let warnok = ref false
let report = ref false

let use_klocalizer = ref true
let check_headers = ref true

