(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

(* Adapted from Coccinelle, GPLv2 *)
val cmd_to_list : string -> string list
val cmd_to_list_and_status : string -> string list * Unix.process_status

val time : string -> (unit -> 'a) -> 'a

val nub : 'a list -> 'a list
val union : 'a list -> 'a list -> 'a list
val inter : 'a list -> 'a list -> 'a list

val mapi : (int -> 'a -> 'b) -> 'a list -> 'b list
val iteri : (int -> 'a -> unit) -> 'a list -> unit

val command : string -> int
val res_command : string -> string list
val stat_command : string -> string list * Unix.process_status
val quiet_command : string -> int
val safe_commands : string list -> int

type ('a,'b) either = Left of 'a | Right of 'b
