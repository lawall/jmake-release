(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

val num_ctr : int ref
val num_fctr : int ref
val sum_ctr : int ref
val sum_fctr : int ref

val engine : string (* patch file *) -> string (* commit *) -> unit
