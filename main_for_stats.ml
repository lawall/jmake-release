(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

type input = Range of string * string | Patch of string | Snapshot
let input = ref (Patch "")
let git_options = ref ""
let ncores = ref 20
let index = ref 0

let options =
  ["--nogit", Arg.Unit (fun _ -> Flags.grep := "grep -r"), "  no git";
    "--config", Arg.Set_string Flags.config, "  configuration name";
    "-j", Arg.Int (fun n -> Flags.cores := Printf.sprintf "-j %d" n),
    "  number of cores available for full kernel compilations";
    "--ncores", Arg.Set_int ncores,
    "  cores for parallelism";
    "--index", Arg.Set_int index,
    "  identity for parallelism";
    "--git-options", Arg.Set_string git_options,
    "  options for commit selection";
    "--snapshot", Arg.Unit (function _ -> input := Snapshot),
    "  obtain arch info about files in the current snapshot";
    "--history", Arg.Set_string Flags.history,
    "  files for which make command is known";
    "--work-history", Arg.Set_string Flags.work_history,
    "  commits for which results are known";
    "--linux", Arg.Set_string Flags.linux, "  linux dir";
    "--debug", Arg.Set Flags.debug, "  debug commands";
    "--exhaustive", Arg.Set Flags.exhaustive, "  continue after failure"]

let anonymous s =
  match Str.split (Str.regexp_string "..") s with
    [starter;ender] -> input := Range (starter,ender)
  | _ -> input := Patch s

let usage = ""

let drop_done_commits commits =
  if not (!Flags.work_history = "")
  then
    begin
      let done_commits =
	Common.cmd_to_list
	  (Printf.sprintf "grep \"Working on \" %s/*tmp"
	     !Flags.work_history) in
      Printf.eprintf "got %d done commits\n" (List.length done_commits);
      let done_commits =
	List.fold_left
	  (fun p x -> (List.nth (Str.split (Str.regexp " ") x) 2) :: p)
	  [] done_commits in
      List.fold_left (fun p x -> if List.mem x done_commits then p else x :: p)
	[] commits
    end
  else commits

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  (match !input with
    Range(starter,ender) ->
      let patch_file = Filename.temp_file "jmake_stats_" ".patch" in
      let args = "-w --diff-filter=M --no-merges" in
      let cmd =
	Printf.sprintf
	  "cd %s; git log %s --pretty=format:\"%%h\" %s %s..%s"
	  !Flags.linux args !git_options starter ender in
      let commits = Common.cmd_to_list cmd in
      let commits = drop_done_commits commits in
      let commits =
	let rec loop n acc = function
	    [] -> acc
	  | x::xs -> loop (n+1) ((n,x)::acc) xs in
	loop 0 [] commits in
      Printf.eprintf "stats todo %d\n" (List.length commits);
      Sys.chdir !Flags.linux;
      List.iter
	(fun (i,commit) ->
	  let _ =
	    Common.command
	      (Printf.sprintf "git show -U0 %s > %s" commit patch_file) in
	  Engine_for_stats.engine patch_file commit)
	commits;
      (if not !Flags.debug then Sys.remove patch_file);
      ()
  | _ -> failwith "not supported");
  Printf.eprintf "at end\n";
  Printf.printf "Average number of mutations per patch: %0.2f\n"
    ((float_of_int !Engine_for_stats.sum_ctr) /.
     (float_of_int !Engine_for_stats.num_ctr));
  Printf.printf "Average number of mutations per file %0.2f\n"
    ((float_of_int !Engine_for_stats.sum_fctr) /.
     (float_of_int !Engine_for_stats.num_fctr))

