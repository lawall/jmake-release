(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

let num_ctr = ref 0
let num_fctr = ref 0
let sum_ctr = ref 0
let sum_fctr = ref 0

let iota a b =
  let rec loop n = if n > b then [] else n::loop (n+1) in
  loop a

let rec skip_to_line ln = function
    [] -> []
  | n::ns -> if ln >= n then skip_to_line ln ns else n::ns

let rec comment_skip_to_line ln = function
    [] -> []
  | (s,n)::ns -> if ln > n then comment_skip_to_line ln ns else (s,n)::ns

let stats = Filename.temp_file "stats" ".c"

let engine patch commit =
  let table = Parse_patch.parse_patch patch in
  Printf.eprintf "%d C files\n" (List.length table);
  let ctr = ref 0 in
  List.iter
    (function (cfile,hunks) ->
      let _ =
	Sys.command
	  (Printf.sprintf "git show %s:%s > %s" commit cfile stats) in
      let cfile = stats in
      try
	let (hash_lines,comment_lines) =
	  Comment_search.comment_search cfile in
	let hash_lines = 0 :: hash_lines in
	let fctr = ref 0 in
	let rec loop hash_lines comment_lines = function
	    (start_line,end_line)::hunks ->
	      let all_lines = iota start_line end_line in
	      let rec iloop hash_lines comment_lines = function
		  ln::rest ->
		    let (incomment,last_comment,comment_lines) =
		      match comment_skip_to_line ln comment_lines with
			[] -> (false,false,[])
		      | ((start_comment,end_comment)::_) as comment_lines ->
			  if ln > start_comment && ln <= end_comment
			  then
			    if ln = end_comment
			    then (true,true,comment_lines)
			    else (true,false,comment_lines)
			  else (false,false,comment_lines) in
		    let hash_lines =
		      if not incomment || last_comment
		      then
			let (danger,hash_lines) =
			  match hash_lines with
			    hash::_ when ln > hash ->
			      (true,skip_to_line ln hash_lines)
			  | hash_lines -> (false,hash_lines) in
			(if danger
			then (ctr := !ctr + 1; fctr := !fctr + 1));
			hash_lines
		      else hash_lines in
		    iloop hash_lines comment_lines rest
		| _ -> (hash_lines,comment_lines) in
	      let (hash_lines,comment_lines) =
		iloop hash_lines comment_lines all_lines in
	      loop hash_lines comment_lines hunks
	  | _ -> () in
	loop hash_lines comment_lines hunks;
	num_fctr := 1 + !num_fctr;
	sum_fctr := !fctr + !sum_fctr
      with Check_compilation.Quit -> ())
    table;
  num_ctr := 1 + !num_ctr;
  sum_ctr := !ctr + !sum_ctr


