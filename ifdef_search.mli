(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

type tests =
    Pos of string | Neg of string | Complex of string | NegComplex of string

val collect : string -> ((int * int * int) * tests) list option

val check_entries : ((int * int * int) * tests) list -> int -> tests list
