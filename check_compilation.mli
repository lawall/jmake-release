(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

exception Quit

val try_compilation : string (* make command *) -> string (* .o file *) ->
  bool (* force *) -> bool

val try_full_compilation : string (* make command *) -> bool

val compile_success : (string,string list ref) Hashtbl.t
val hashadd : ('a,'b list ref) Hashtbl.t -> 'a -> 'b -> unit

val clear_config : unit -> unit
val change_config : string -> int

val get_make : string (* .c file *) ->
  string (* make command *) * string option (* sample file *)
