(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

val parse_patch : string (* patch file *) ->
  (string (* file name *) * (int * int) list (* + lines *)) list
