(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

let hash1 = Str.regexp "[ \t]*#[ \t]*if"
let hash2 = Str.regexp "[ \t]*#[ \t]*else"
let hash3 = Str.regexp "[ \t]*#[ \t]*elif"
let hash_define = Str.regexp "[ \t]*#[ \t]*define"
let backslash = Str.regexp "\\\\[ \t]*"
let c_start = Str.regexp_string "/*"
let c_end = Str.regexp_string "*/"

let have_comment_end s =
  try let _ = Str.search_forward c_end s 0 in true
  with Not_found -> false

let ends_with_continuation line =
  let checkback _ =
    match List.rev (Str.split_delim backslash line) with
      ""::before::_ -> true
    | _ -> false in
  match List.rev (Str.split_delim c_start line) with
    last::_::_ ->
      let completed = have_comment_end last in
      if not completed
      then false
      else checkback()
  | _ -> checkback()

let get_macro_name line =
  match Str.split_delim hash_define line with
    _::second::_ ->
      (match Str.split (Str.regexp "[ \t(]+") second with
	name::_ -> name
      | _ -> failwith "macro with no name")
  | _ -> failwith "no name"

let search i =
  let hash_lines = ref [] in
  let macro_lines = ref [] in
  let define_lines = ref [] in
  let c_comment_ranges = ref [] in
  let n = ref 0 in
  let rec loop name =
    let line = input_line i in
    n := !n + 1;
    (if ends_with_continuation line
    then macro_lines := (!n,name) :: !macro_lines);
    let name =
      if Str.string_match hash_define line 0
      then
	begin
	  let name = get_macro_name line in
	  define_lines := (!n,name) :: !define_lines;
	  Some name
	end
      else name in
    (if (Str.string_match hash1 line 0) || (Str.string_match hash2 line 0) ||
      (Str.string_match hash3 line 0) 
    then hash_lines := !n :: !hash_lines
    else
      match List.rev (Str.split_delim c_start line) with
	last::_::_ ->
	  let completed = have_comment_end last in
	  if not completed
	  then read_to_end !n
      | _ -> ());
    loop name
  and read_to_end start_line =
    let line = input_line i in
    n := !n + 1;
    match List.rev (Str.split_delim c_start line) with
      last::_::_ ->
	c_comment_ranges := (start_line,!n) :: !c_comment_ranges;
	let completed = have_comment_end last in
	if not completed
	then read_to_end !n
    | _ ->
	let completed = have_comment_end line in
	if completed
	then c_comment_ranges := (start_line,!n) :: !c_comment_ranges
	else read_to_end start_line in
  try loop None
  with End_of_file ->
    (0 :: (List.rev !hash_lines),List.rev !macro_lines,
     List.rev (((!n+1),"") :: !define_lines),
     List.rev !c_comment_ranges)

let comment_search cfile =
  let i = open_in cfile in
  let res = search i in
  close_in i;
  res
