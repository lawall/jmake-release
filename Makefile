# This file is part of JMake, lincensed under the terms of the GPL v2.
# See copyright.txt in the JMake source code for more information.
# The JMake source code can be obtained at http://jmake-release.gforge.inria.fr

TARGET=jmake
SRC=flags.ml common.ml bad.ml ifdef_search.ml redo_check_compilation.ml \
comment_search.ml redo_mutate.ml parse_patch.ml redo_engine.ml redo_main.ml

INCLUDEDIRS=

SYSLIBS=str.cma unix.cma
OPTSYSLIBS=$(SYSLIBS:.cma=.cmxa)

OBJS=    $(SRC:.ml=.cmo)
OPTOBJS= $(SRC:.ml=.cmx)

EXEC=$(TARGET)
OPTEXEC=$(TARGET).opt

INCLUDES=$(INCLUDEDIRS:%=-I %)
OCAMLCFLAGS=-g
OCAMLC=ocamlc $(OCAMLCFLAGS) $(INCLUDES)
OCAMLOPT=ocamlopt $(OPTFLAGS) $(INCLUDES)
OCAMLDEP=ocamldep

all: $(EXEC)
opt: $(OPTEXEC)

$(EXEC): $(OBJS)
	$(OCAMLC) -o $(EXEC) $(SYSLIBS) $(OBJS)
$(OPTEXEC): $(OPTOBJS)
	$(OCAMLOPT) -o $(OPTEXEC) $(OPTSYSLIBS) $(OPTOBJS)

.SUFFIXES:
.SUFFIXES: .ml .mli .cmo .cmi .cmx

.ml.cmo:
	$(OCAMLC) -c $<

.mli.cmi:
	$(OCAMLC) -c $<

.ml.cmx:
	$(OCAMLOPT) -c $<

# clean rule for others files
clean::
	rm -f *.cm[iox] *.o 
	rm -f *~ .*~ #*# 
	rm -f .depend

depend: 
	$(OCAMLDEP) *.mli *.ml > .depend

.depend:
	$(OCAMLDEP) *.mli *.ml > .depend

include .depend
