(*
 * This file is part of JMake, lincensed under the terms of the GPL v2.
 * See copyright.txt in the JMake source code for more information.
 * The JMake source code can be obtained at http://jmake-release.gforge.inria.fr
 *)

(* arguments: target file, target line, output port *)
val non_comment_line_mutate : string -> int -> out_channel -> unit
val comment_line_mutate :
    string (* patch *) -> string -> int -> out_channel -> unit
